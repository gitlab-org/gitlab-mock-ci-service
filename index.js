const express = require('express');

const app = express();

app.get("*", function(req, res, next) {
  res.json({
    // "failed", "canceled", "running", "pending", "success", "success_with_warnings", "skipped", "not_found"
    status: 'running'
  })
});

app.listen(4004);
