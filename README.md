# GitLab Mock CI Service

See https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9250

```
npm start
```


# Setup MockCI Integration

Make sure your GDK instance is set to the development environment, `Rails.env.development?`

Go to Project settings -> Integrations -> Project Services -> MockCi

![](https://i.imgur.com/Wtgroro.png)

Check the "Active" checkbox and set the "Mock service url" (`http://localhost:4004` is default for this service)

![](https://i.imgur.com/hvuMkDw.png)


# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
